import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {Storage} from '@ionic/storage';

/*
 Generated class for the CloudImageViewer page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-cloud-image-viewer',
  templateUrl: 'cloud-image-viewer.html'
})
export class CloudImageViewerPage {


  imageID: string;


  constructor(public navCtrl: NavController, public viewCtrl: ViewController,
              public navParams: NavParams,
              public storage: Storage,
              public navPar: NavParams) {

    this.imageID = this.navPar.get('URL');
    console.log(URL);
    console.log(this.imageID);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CloudImageViewerPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }


}
