import {Component, NgZone} from '@angular/core';

import {NavController, Platform} from 'ionic-angular';
import {Camera} from "ionic-native";
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';
import {ImagesCloudPage} from "../images-cloud/images-cloud";

declare var window: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  firebaseImages: any;
  images: any = {};
  theImage: string;
  public Image;
  public files = [];
  assetCollection: any;



  constructor(public navCtrl: NavController,public storage: Storage,
              public ngzone: NgZone, public platform: Platform) {
    this.ngzone = ngzone;

    this.firebaseImages = [];

    // let config = {
    //   apiKey: "AIzaSyAsRgQpVjaVGM5TsYeQeczxd3CmLaCak1g",
    //   authDomain: "appprog-c3b1d.firebaseapp.com",
    //   databaseURL: "https://appprog-c3b1d.firebaseio.com",
    //   storageBucket: "appprog-c3b1d.appspot.com",
    //   messagingSenderId: "31691376413"
    // };
    //
    // firebase.initializeApp(config);
    //
    //
    // firebase.auth().signInAnonymously()
    //   .then((auth) => {
    //     console.log('login success');
    //     this.loadData();
    //
    //   })
    //   .catch((e) => {
    //     // var errorCode = e.code;
    //     var errorMessage = e.message;
    //
    //     console.log(errorMessage+'You are not connected to internet');
    //   });

    this.images = {};
    this.images.imagesArr = [];

    this.storage.get('testApp.images').then( (value:any) => {

      if(!value){
        this.storage.set('testApp.images', {
          count: 1,
          imagesArr: []
        });
      }else{
        this.images = value;
      }

    });

  }
  // loadData() {
  //   // load data from firebase...
  //   firebase.database().ref('assets').on('value', (_snapshot: any) => {
  //     var result = [];
  //
  //     _snapshot.forEach((_childSnapshot) => {
  //       // get the key/id and the data for display
  //       var element = _childSnapshot.val();
  //       element.id = _childSnapshot.key;
  //
  //        result.push(element);
  //       //this.firebaseImages.push(element);
  //     });
  //
  //     // set the component property
  //     this.assetCollection = result;
  //   });
  // }

  makeFileIntoBlob(_imagePath) {

    // INSTALL PLUGIN - cordova plugin add cordova-plugin-file
    if (this.platform.is('android')) {
      return new Promise((resolve, reject) => {
        window.resolveLocalFileSystemURL(_imagePath, (fileEntry) => {

          fileEntry.file((resFile) => {

            var reader = new FileReader();
            reader.onloadend = (evt: any) => {
              var imgBlob: any = new Blob([evt.target.result], { type: 'image/jpeg' });
              imgBlob.name = 'sample.jpg';
              resolve(imgBlob);
            };

            reader.onerror = (e) => {
              console.log('Failed file read: ' + e.toString());
              reject(e);
            };

            reader.readAsArrayBuffer(resFile);
          });
        });
      });
    }
  }

  uploadToFirebase(_imageBlob) {
    var fileName = 'sample-' + new Date().getTime() + '.jpg';

    return new Promise((resolve, reject) => {
      var fileRef = firebase.storage().ref('images/' + fileName);

      var uploadTask = fileRef.put(_imageBlob);

      uploadTask.on('state_changed', (_snapshot) => {
        console.log('snapshot progess ' + _snapshot);
      }, (_error) => {
        reject(_error);
      }, () => {
        // completion...
        resolve(uploadTask.snapshot);
      });
    });
  }

  saveToDatabaseAssetList(_uploadSnapshot) {
    var ref = firebase.database().ref('assets');

    return new Promise((resolve, reject) => {

      // we will save meta data of image in database
      var dataToSave = {
        'URL': _uploadSnapshot.downloadURL, // url to access file
        'name': _uploadSnapshot.metadata.name, // name of the file
        'owner': firebase.auth().currentUser.uid,
        'email': firebase.auth().currentUser.email,
        'lastUpdated': new Date().getTime(),
      };

      ref.push(dataToSave, (_response) => {
        resolve(_response);
      }).catch((_error) => {
        reject(_error);
      });
    });

  }


  doGetPicture() {
    // TODO:
    // get picture from camera
    Camera.getPicture({
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.CAMERA,
      encodingType: Camera.EncodingType.JPEG,
      quality: 50,
      correctOrientation: true,
      saveToPhotoAlbum: false,
      allowEdit: false,

    }).then((_imagePath) => {
      this.theImage=_imagePath;
      this.storage.get('testApp.images').then( (value:any) => {

        this.images = value;

        var image = {
          id: value.count,
          src: this.theImage
        };

        this.images.imagesArr.push(image);

        this.images.count++;

        this.storage.set('testApp.images', this.images);

      });
      console.log('got image path ' + _imagePath);
      // convert picture to blob
      return this.makeFileIntoBlob(_imagePath);
    }).then((_imageBlob) => {
      console.log('got image blob ' + _imageBlob);

      // upload the blob
      return this.uploadToFirebase(_imageBlob);
    }).then((_uploadSnapshot: any) => {
      console.log('file uploaded successfully  ' + _uploadSnapshot.downloadURL);

      // store reference to storage in database
      return this.saveToDatabaseAssetList(_uploadSnapshot);

    }).then((_uploadSnapshot: any) => {
      console.log('file saved to asset catalog successfully  ');
    }, (_error) => {
      console.log('Error ' + _error.message);
    });



  }


}

//this.makeFileIntoBlob(pic).then(blob => {
// return this.uploadToFirebase(blob);    // File or Blob named mountains.jpg
//
// })

