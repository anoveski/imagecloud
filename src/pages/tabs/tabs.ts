import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import {GalleryPage} from "../gallery/gallery";
import {ImagesCloudPage} from "../images-cloud/images-cloud";
import {MapPage} from "../map/map";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = HomePage;
  tab2Root: any = ImagesCloudPage;
  tab3Root: any = GalleryPage;
  tab4Root: any = MapPage;
  constructor() {

  }
}
