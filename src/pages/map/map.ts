import {Component} from '@angular/core';

import {NavController, Platform} from 'ionic-angular';
import {Geolocation, GoogleMapsLatLng} from "ionic-native";

import {GoogleMap, GoogleMapsEvent} from 'ionic-native';

declare var google;

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {
  watchId: any;
  search: string;
  map: any;

  constructor(public navCtrl: NavController, platform: Platform) {

    platform.ready().then(() => {

      Geolocation.getCurrentPosition({}).then((resp) => {


        let myPosition: GoogleMapsLatLng = new GoogleMapsLatLng(resp.coords.latitude, resp.coords.longitude);
        let Ohrid: GoogleMapsLatLng = new GoogleMapsLatLng(41.111127, 20.788657);
        let waterfall: GoogleMapsLatLng = new GoogleMapsLatLng(41.365734, 22.899557);
        let bulgaria: GoogleMapsLatLng = new GoogleMapsLatLng(42.133607, 23.340089);
        let montenegro: GoogleMapsLatLng = new GoogleMapsLatLng(42.255600, 18.891263);
        let skopjeCitadel: GoogleMapsLatLng = new GoogleMapsLatLng(42.004278, 21.432817);


        let opt = {
          'backgroundColor': 'white',
          'controls': {
            'compass': true,
            'myLocationButton': true,
            'indoorPicker': true,
            'zoom': true
          },
          'gestures': {
            'scroll': true,
            'tilt': true,
            'rotate': true,
            'zoom': true
          },
          'camera': {
            'latLng': myPosition,
            'zoom': 14
          }

        };

        this.map = new GoogleMap('map', opt);


        this.map.on(GoogleMapsEvent.MAP_READY).subscribe(() => {

          this.map.addMarker({
            'position': myPosition,
            'title': "Your current position"
          }).then(function (data) {
            console.log('MARKER IS SHOWN');
          });
          this.map.addMarker({
            'position': Ohrid,
            'title': "Here is Kaneo",
            'icon': {
              url: 'http://www.becgworkshop.com/wp-content/uploads/2015/04/camera-1.png'
            },
            infoWindowImage: "https://media-cdn.tripadvisor.com/media/photo-s/0c/5f/7d/47/img-20160804-104015-largejpg.jpg" //AKO NE RABOTAT STAJ EDINECNI NAVODNICI
          }).then(function (data) {

          });
          this.map.addMarker({
            'position': bulgaria,
            'title': "Rila Monastery- Bulgaria Monasteri",
            'icon': {
              url: 'http://www.clker.com/cliparts/K/n/V/6/W/0/camera-icon-th.png'
            }
          }).then(function (data) {

          });
          this.map.addMarker({
            'position': waterfall,
            'title': "Macedonian waterfall",
            'icon': {
              url: 'http://www.clker.com/cliparts/7/i/i/R/9/H/camera-fotografica-rosa-th.png'
            }
          }).then(function (data) {

          });
          this.map.addMarker({
            'position': montenegro,
            'title': "Sv.Stefan Montenegro",
            'icon': {
              url: 'https://upload.wikimedia.org/wikipedia/en/thumb/7/78/Google_Camera_Icon.png/100px-Google_Camera_Icon.png'
            },
            infoWindowImage: "http://3.bp.blogspot.com/-c9P3tdcagyM/UeJ3TmH7CLI/AAAAAAAAV6k/_xvwA1fUwfY/s640/AMAN+SVETI+STEFAN+RESORT,+MONTENEGRO+18.jpg" //AKO NE RABOTAT STAJ EDINECNI NAVODNICI
          }).then(function (data) {

          });
          this.map.addMarker({
            'position': skopjeCitadel,
            'title': "Skopje Fortress",
            'icon': {
              url: 'https://upload.wikimedia.org/wikipedia/en/thumb/7/78/Google_Camera_Icon.png/100px-Google_Camera_Icon.png'
            },
            infoWindowImage: "http://3.bp.blogspot.com/-c9P3tdcagyM/UeJ3TmH7CLI/AAAAAAAAV6k/_xvwA1fUwfY/s640/AMAN+SVETI+STEFAN+RESORT,+MONTENEGRO+18.jpg" //AKO NE RABOTAT STAJ EDINECNI NAVODNICI
          }).then(function (data) {

          });
        });
      });

      let watch = Geolocation.watchPosition();
      watch.subscribe((data) => {

      });

    });

  }

  ionViewDidLoad() {
    console.log('Hello MapPage Page');
  }


}



