import {Component} from '@angular/core';
import {NavController, NavParams, ModalController, ActionSheetController} from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {SocialSharing} from "ionic-native";
import {ImagePage} from "../../modals/image/image";


/*
  Generated class for the Gallery page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-gallery',
  templateUrl: 'gallery.html'
})
export class GalleryPage {

  images: any = {};



  constructor(public navCtrl: NavController,
              public storage: Storage,
              public modalCtrl: ModalController,
              public actionSheetCtrl: ActionSheetController) {

      this.images = {};
      this.images.imagesArr = [];

    console.log("Reload");
    this.storage.get('testApp.images').then((value: any) => {

      if (!value) {
        this.storage.set('testApp.images', {
          count: 1,
          imagesArr: []
        });
      } else {
        this.images = value;
      }

    });

  }

  presentActionSheet(id) {

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Modify your album',
      buttons: [
        {
          text: 'Delete',
          role: 'destructive',
          handler: () => {
            // we gonna delete the photo here
            this.storage.get('testApp.images').then( (value:any) => {

              this.images = value;

              this.images.imagesArr = this.images.imagesArr.filter(function(jsonObject) {
                return jsonObject.id != id;
              });

              this.storage.set('testApp.images', this.images);

            });
          }
        },{
          text: 'Share on Facebook',
          handler: () => {

            this.storage.get('testApp.images').then( (value:any) => {

              this.images = value;

              for(var i=0; i< this.images.imagesArr.length; i++){

                if(this.images.imagesArr[i].id == id){
                  SocialSharing.shareViaFacebook('This is awesome picture', this.images.imagesArr[i].src, null).then(() => {
                    // Success!
                  });
                }

              }

            });
          }

        },
        {
          text: 'Share on Instagram',
          handler: () => {
            this.storage.get('testApp.images').then( (value:any) => {

              this.images = value;

              for(var i=0; i< this.images.imagesArr.length; i++){

                if(this.images.imagesArr[i].id == id){
            SocialSharing.shareViaInstagram('My picture', this.images.imagesArr[i].src).then(() =>{
              // Success!
            });
                }

              }

            });
          }
        },{
          text: 'Share on WhatsApp',
          handler: () => {
            this.storage.get('testApp.images').then( (value:any) => {

              this.images = value;

              for(var i=0; i< this.images.imagesArr.length; i++){

                if(this.images.imagesArr[i].id == id){
            SocialSharing.shareViaWhatsApp('My picture',this.images.imagesArr[i].src,null).then(()=>{
              // Success!
            });
                }

              }

            });
          }

        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad GalleryPage');
  }


  openFullSizeImg(id){

    // let imgModal = this.modalCtrl.create(ImagePage, { id: id });
    let imgModal = this.modalCtrl.create(ImagePage, { id: id });
    imgModal.present();

  }

}
