import { Component } from '@angular/core';
import {NavController, NavParams, Platform, ModalController, ActionSheetController} from 'ionic-angular';
import * as firebase from 'firebase';
import {CloudImageViewerPage} from "../../modals/cloud-image-viewer/cloud-image-viewer";
import {SocialSharing, Facebook} from "ionic-native";


/*
 Generated class for the ImagesCloud page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-images-cloud',
  templateUrl: 'images-cloud.html'
})
export class ImagesCloudPage {

 // firebaseImages: any;
  images: any = {};

  public Image;
  public files = [];
  assetCollection: any;




  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public platform: Platform,
              public modalCtrl: ModalController,
              public actionSheetCtrl: ActionSheetController) {

   // this.firebaseImages = [];


    let config = {
      apiKey: "AIzaSyAsRgQpVjaVGM5TsYeQeczxd3CmLaCak1g",
      authDomain: "appprog-c3b1d.firebaseapp.com",
      databaseURL: "https://appprog-c3b1d.firebaseio.com",
      storageBucket: "appprog-c3b1d.appspot.com",
      messagingSenderId: "31691376413"
    };

    firebase.initializeApp(config);



    firebase.auth().signInAnonymously()
      .then((auth) => {
        console.log('login success');
        this.loadData();

      })
      .catch((e) => {
        // var errorCode = e.code;
        var errorMessage = e.message;

        console.log(errorMessage+'You are not connected to internet');
      });
  }

  loadData() {
    // load data from firebase...
    firebase.database().ref('assets').on('value', (_snapshot: any) => {
      var result = [];

      _snapshot.forEach((_childSnapshot) => {
        // get the key/id and the data for display
        var element = _childSnapshot.val();
        element.id = _childSnapshot.key;

        result.push(element);
        //this.firebaseImages.push(element);
      });

      // set the component property
      this.assetCollection = result;
    });
  }
  openFullSizeImgCloud(URL){
    console.log(URL);

    let imgModal = this.modalCtrl.create(CloudImageViewerPage, { URL });
    imgModal.present();
}
  presentActionSheetCloud(URL) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Share picture',
      buttons: [
        {
        text: 'Share on Facebook',
          handler: () => {
            SocialSharing.shareViaFacebook('This is awesome picture',null, URL).then(() => {
              console.log(URL);
              // Success!
            });
          }


    },{
        text: 'Share on Instagram',
          handler: () => {
           SocialSharing.shareViaInstagram('My picture', URL).then(() =>{
              // Success!
             });
        }
        },{
          text: 'Share on WhatsApp',
          handler: () => {
            SocialSharing.shareViaWhatsApp('My picture',null,URL).then(()=>{
              // Success!
            });
          }

        },

        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
    ]
    });
    actionSheet.present();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ImagesCloudPage');
  }

}
