import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ImagesCloudPage} from '../pages/images-cloud/images-cloud';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import {Storage} from "@ionic/storage";
import {GalleryPage} from "../pages/gallery/gallery";
import {ImagePage} from "../modals/image/image";
import {MapPage} from "../pages/map/map";
import {CloudImageViewerPage} from "../modals/cloud-image-viewer/cloud-image-viewer";


@NgModule({
  declarations: [
    MyApp,
    MapPage,
    ImagesCloudPage,
    HomePage,
    TabsPage,
    GalleryPage,
    ImagePage,
    CloudImageViewerPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapPage,
    ImagesCloudPage,
    HomePage,
    TabsPage,
    GalleryPage,
    ImagePage,
    CloudImageViewerPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, Storage]


})
export class AppModule {}
